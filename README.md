avg\_image - calculate the average frame of a film
==================================================

Just as the title says, this small programm uses opencv to
calculate the average frame of a given film. While not really
usefull, it is interessting to see how a dark movie results in
darker images or extremeley bloody movies end up with a slight
red (for a quite strong example of the later, let it run on
the seventh episode of Hellsing Ultimate, which is basically
a one hour long blood-bath).

Usage
-----

    avg_image <input_file> <output_file> [skip]

* `input_file` has to be a film in a format that opencv can read.
* `output_file` is the location where the generated image will be saved.
* `skip` is an optional unsigned integer that specifies that only every
  n'th frame should be taken into the calculation. In theory this might
  be a great time-safer but currently it almost doesn't help at all,
  due to the way that opencv is used to read the movie.


License
-------

GPLv3
