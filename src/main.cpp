
#include <iostream>
#include <string>
#include <exception>
#include <cstdint>

#include <opencv2/opencv.hpp>

int main(int argc, char** argv) try {
	if (argc < 3 or argc > 4) {
		std::cerr << "Error: invalid number of arguments.\n"
		             "Usage: " << argv[0] << " <film> <output> [skip]\n";
		return 1;
	}
	cv::VideoCapture video{argv[1]};
	cv::Mat current;
	video.grab() && video.retrieve(current);
	cv::Mat sum{current.size(), CV_64FC3};
	std::cout << "Video-dimensions: " << current.size().width << "x" << current.size().height
	          << ", " << current.channels() << " channels\n";
	cv::add(sum, current, sum, cv::noArray(), CV_64FC3);
	std::size_t num_images = 1u;
	const auto frame_skip = argc > 3 ? std::stoul(argv[3]) : 0ul;
	while (video.grab() && video.retrieve(current)) {
		++num_images;
		cv::add(sum, current, sum, cv::noArray(), CV_64FC3);
		for (auto i = 0ul; i < frame_skip; ++i) {
			if (!video.grab()) {
				break;
			}
		}
	}
	std::cout << "summed up " << num_images << " images\n";
	sum /= num_images;
	cv::imwrite(argv[2], sum);

} catch (std::exception& e) {
	std::cerr << "Error: " << e.what() << '\n';
	return 2;
}
